import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
//Se importa la carpeta, para realizar la navegación y aplicar cambios
import { solicitaIncidencia } from '../pages/solicitaIncidencia/solicitaIncidencia';
import { eligirIncidencia } from '../pages/eligirIncidencia/eligirIncidencia';
import { salida } from '../pages/incidenciaSalida/salida';
import { lista } from '../pages/listaIncidencia/lista';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    solicitaIncidencia, //Se declara solicitaIncidencia para poder realizar la navegación
    eligirIncidencia, 
    salida,
    lista
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    solicitaIncidencia, //Se agrega a los componentes 
    eligirIncidencia,
    salida,
    lista
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
