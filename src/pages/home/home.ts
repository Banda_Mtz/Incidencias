import { Component } from '@angular/core';

//Se importa la alerta para ver el acceso 
import { NavController, AlertController, Events } from 'ionic-angular';
//Se importa lo de la clase solicitaIncidencia junto con la carpeta que contiene el proyecto para llamar al .ts
import {eligirIncidencia} from '../eligirIncidencia/eligirIncidencia';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController, public alerCtrl: AlertController, public events: Events) {}

  //Metodo para manejar la alerta
  alertaAcceso(){
    let alert = this.alerCtrl.create({
      title: 'Ingreso', //Titulo de la alerta 
      message: 'Acceso correcto', //El mensaje de la alerta 
      buttons: ['Entendido']     //Mensaje que va contener el boton del la 
    
  });
    alert.present()
    this.navCtrl.setRoot(eligirIncidencia) //Se realiza un push para pasar a la otra página
  }

} 