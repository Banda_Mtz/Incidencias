import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
//Se importa el toast para mandar un mensaje
import { ToastController } from 'ionic-angular';


@Component({
  selector: 'page-incidenciaSalida',
  templateUrl: 'salida.html'
})

  export class salida {
    now: Date;
    constructor(public navCtrl: NavController, public toastCtrl: ToastController) {
      //Este parte de la navegación permite desaparecer la primera ventana
      //this.navCtrl.setRoot(solicitaIncidencia);
      setInterval(() => this.now = new Date());
  }
    //Datos de fecha de incio, hora y fecha final.
  public data = {
    month: '2017-01-01',
    timeStarts: '07:00',
    timeEnds: '2017-01-01'
  }

  //Datos donde iran los detalles de la incidencia
  public detallesIncidencia = {
    nombreEmpleado: 'Luis Aguila Hernandez Martínez',
    numeroEmpleado: '7778884412',
    status: 'Activo',
    tipoIncidencia: 'Tiempo completo'
  }

  //Metodo que permite mandar el mensaje.
  solicitar() {
    //Se crea una constante que permite manipular el control del mensaje.
    const toast = this.toastCtrl.create({
      message: 'Tú solicitud se ha realizado con éxito.', //Mensaje de la ventana.
      showCloseButton: true, //Se muestra el botón.
      closeButtonText: 'Ok' //Se pone un mensaje al botón para que se cierre el mensaje.
    });
    toast.present(); //Se visualiza en la misma ventana.
}

}
