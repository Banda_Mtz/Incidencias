import { Component } from '@angular/core';

import { NavController} from 'ionic-angular';

//Se importa el toast para mandar un mensaje
import { ToastController } from 'ionic-angular';
//Se importa la clase solicitaIncidencia para hacer de página
import { solicitaIncidencia } from '../solicitaIncidencia/solicitaIncidencia';
//Salida
import { salida } from '../incidenciaSalida/salida';
//Lista
import { lista } from '../listaIncidencia/lista';

@Component({
  selector: 'page-home',
  templateUrl: 'eligirIncidencia.html'
})

export class eligirIncidencia {
  //eligirIncidencias: any; // Para crear el array
 
  constructor(public navCtrl: NavController, public toastCtrl: ToastController) {
    //Este parte de la navegación permite desaparecer la primera ventana
    //this.navCtrl.setRoot(solicitaIncidencia);
/*
  this.eligirIncidencias = [
    {incidencia: "Entrada", titulo: "Entrada"},
    {incidencia: "Salida", titulo: "Salida"},
    {incidencia: "Otra incidencia", titulo: "Otra incidencia"}
  ];
 */
}

  //Metodo que permite la navegación   
 /* solicitar() {

    if(this.eligirIncidencias.titulo === "Salida"){
      this.navCtrl.push(salida)
    }
  }
*/


  entrada(){
    this.navCtrl.push(solicitaIncidencia)
  }
  salida(){
    this.navCtrl.push(salida)
  }
  otraIncidencia(){
    this.navCtrl.push(solicitaIncidencia)
  }

  listaServ(){
      this.navCtrl.push(lista)
  }
  

}
